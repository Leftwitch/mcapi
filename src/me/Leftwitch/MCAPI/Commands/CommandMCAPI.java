package me.Leftwitch.MCAPI.Commands;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Utils.PlayerUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMCAPI implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String cmdLabel, String[] args) {
		if (args.length == 0) {
			sender.sendMessage("�7|> �6---------- �3McAPI �6---------- �7<|");
			sender.sendMessage("�6McAPI Von �3"
					+ MCAPI.instance.getDescription().getAuthors().get(0));
			sender.sendMessage("�6McAPI Version: �3"
					+ MCAPI.instance.getDescription().getVersion());
			sender.sendMessage("�6--------------------------------");
		} else if (args.length == 2) {
			String arg = args[0];
			if (sender.isOp()) {
				if (arg.equalsIgnoreCase("update")) {
					update(args[1], sender);
				}
			} else {
				sender.sendMessage("�4Keine Berechtigung");
			}
		} else {
			sender.sendMessage("�7|> �6---------- �3McAPI �6---------- �7<|");
			sender.sendMessage("�6McAPI Von �3"
					+ MCAPI.instance.getDescription().getAuthors().get(0));
			sender.sendMessage("�6McAPI Version: �3"
					+ MCAPI.instance.getDescription().getVersion());
			sender.sendMessage("�6--------------------------------");
		}
		return true;
	}

	/**
	 * 
	 * @param urllink URL Zum Updaten
	 * @param sender Command Sender
	 */
	private void update(final String urllink, final CommandSender sender) {
		for (int i = 0; i < 70; i++)
			sender.sendMessage("");
		sender.sendMessage("�8[�6McAPI�8] �aUpdate-Prozess Gestartet");
		sender.sendMessage("�8[�6McAPI�8] �aRufe Information Von url ab: ");
		sender.sendMessage("�8[�6McAPI�8] �a" + urllink);
		for (int i = 0; i < 7; i++)
			sender.sendMessage("");

		Thread t = new Thread() {
			public void run() {
				try {
					URL url = new URL(urllink);
					final URLConnection con = url.openConnection();
					InputStream in = con.getInputStream();
					OutputStream out = new FileOutputStream(
							"plugins/McAPI2.jar");
					byte[] buffer = new byte[1024];
					long total = 0;
					for (int n; (n = in.read(buffer)) != -1;) {
						out.write(buffer, 0, n);
						total += n;

						String percent = ""
								+ (int) ((total * 100) / con.getContentLength());
						if (!(sender instanceof Player)) {
							for (int i = 0; i < 20; i++)
								sender.sendMessage("");
							sender.sendMessage("�8[�6McAPI�8] �aUpdate-Prozess Gestartet");
							sender.sendMessage("�8[�6McAPI�8] �aRufe Information Von url ab: ");
							sender.sendMessage("�8[�6McAPI�8] �a" + urllink);
							for (int i = 0; i < 7; i++)
								sender.sendMessage("");

							sender.sendMessage("�8-----------------------------------");
							sender.sendMessage("�8Downloade: �6" + percent
									+ "%");
						} else {
//							PlayerUtils.sendTitle((Player) sender,
//									"�6�n�oDownloade Update",
//									"�5Mc-Api Wird Geupdated: �7" + percent
//											+ "%");
						}
						if (sender instanceof Player) {
							Player p = (Player) sender;
							p.setLevel(Integer.parseInt(percent));
						}
					}

					in.close();
					out.close();
//					PlayerUtils.sendTitle((Player) sender,
//							"�2�n�oDownloade Erfolgreich",
//							"�aMc-Api Wurde Erfolgreich Geupdated!");
//					sender.sendMessage("�8Update Erfolgreich");

					if (sender instanceof Player) {
						Player p = (Player) sender;
						p.setLevel(0);
					}

				} catch (Exception error) {
					sender.sendMessage("�4Update Nicht Gefunden!");

				}
			}
		};

		t.start();

	}

}
