package me.Leftwitch.MCAPI.Utils;

import net.minecraft.server.v1_8_R1.PacketPlayOutNamedSoundEffect;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class SoundUtils {
	
   /**
    * 
    * @param p Spieler
    * @param soundName Sound Pfad z,b vom resource pack "mob.ambient.hurt" 
    * @param range weite
    * @param pitch wie hoch
    */
	public void playSound(Player p,String soundName,int range,int pitch){
		   PacketPlayOutNamedSoundEffect packet = new PacketPlayOutNamedSoundEffect(soundName, p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), range,pitch);
		   CraftPlayer cp = (CraftPlayer)p;
		   cp.getHandle().playerConnection.sendPacket(packet);
		}
}
