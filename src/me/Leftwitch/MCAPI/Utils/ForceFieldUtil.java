package me.Leftwitch.MCAPI.Utils;

import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.DataClasses.ForceField;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ForceFieldUtil extends MCAPI {
	public static ForceField createForceField(Location loc, int range, Effect ef) {
		ForceField ff = new ForceField();
		ff.setCenter(loc);
		ff.setEf(ef);
		ff.setRange(range);
		MapGrenze mapGrenze = MapGrenze.createGrenze(loc, range);
		ff.setMapgrenze(mapGrenze);
		ff.setEnabled(true);
		values.getActiveForceFields().add(ff);
		return ff;

	}

	public static void destroyForceField(ForceField f) {
		f.setEnabled(false);
		values.getActiveForceFields().remove(f);
	}

	public static void startForcefieldScheduler() {
		Bukkit.getScheduler().runTaskTimer(MCAPI.instance, new Runnable() {

			@Override
			public void run() {
				if (values.getActiveForceFields().size() > 0) {
					for (ForceField ff : values.getActiveForceFields()) {

						for (Player ps : Bukkit.getOnlinePlayers()) {
                         if(ps.getLocation().distance(ff.getCenter())>=(ff.getRange()-7)){
                        	for(Location loc : ff.getMapgrenze().grenzen){
                        		if(loc.distance(ps.getLocation())<=7){
                        		   ps.playEffect(loc, ff.getEf(), 5);
                        		}
                        	}
                         }
						}
					}
				}

			}
		}, 0, 10);
	}
}
