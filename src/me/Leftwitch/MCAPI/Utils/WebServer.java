package me.Leftwitch.MCAPI.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class WebServer {
boolean running = false;

	  public void start(File f,int port) {
	    ServerSocket s;
	    try {
	      // create the main server socket
	      s = new ServerSocket(port);
	    } catch (Exception e) {
	      System.out.println("Error: " + e);
	      return;
	    }

	    System.out.println("Warte auf verbindungen...");
	    while(running) {
	      try {
	        // wait for a connection
	        Socket remote = s.accept();
	        // remote is now the connected socket
	        System.out.println("Verbindung... Sende daten");
	        BufferedReader in = new BufferedReader(new InputStreamReader(
	            remote.getInputStream()));

	        PrintWriter out = new PrintWriter(remote.getOutputStream());
      
	        // Send the response
	        // Send the headers
	        out.println("HTTP/1.0 200 OK");
	        out.println("Content-Type: text/html");
	        out.println("Server: Bot");
	        // this blank line signals the end of the headers
	        out.println("");
	        // Send the HTML page
            for(String str : getLinesFromInputStream(new FileInputStream(f))){
	        out.print(str);
            }
	        out.flush();
            String all = in.readLine();
            if(all!=null){
	        String file = all.split(" ")[1];
	        System.out.println(file);
            }
	        remote.close();
	      } catch (Exception e) {
	        System.out.println("Error: " + e);
	      }
	    }
	  }
	  
	  
	  public void stop(){
		  running=false;
	  }
	
  	  public  List<String> getLinesFromInputStream(InputStream is){
  		  List<String> lines = new ArrayList<>();
  		  try {
  				BufferedReader in = new BufferedReader(new InputStreamReader(is));
  				String zeile = null;
  				while ((zeile = in.readLine()) != null) {
  					lines.add(zeile);
  				}
  			} catch (IOException e) {
  				e.printStackTrace();
  			}
  		return lines;
  				
  	  }
	  
}
