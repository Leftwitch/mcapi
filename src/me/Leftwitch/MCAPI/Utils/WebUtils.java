package me.Leftwitch.MCAPI.Utils;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import lombok.Data;

public class WebUtils {

	public static DownloadState downloadFileAsync(final String urllink,
			final String path) {
		final DownloadState state = new DownloadState();
		Thread t = new Thread() {
			public void run() {
				try {
					URL url = new URL(urllink);
					final URLConnection con = url.openConnection();
					InputStream in = con.getInputStream();
					OutputStream out = new FileOutputStream(path);
					byte[] buffer = new byte[1024];
					long total = 0;
					for (int n; (n = in.read(buffer)) != -1;) {
						out.write(buffer, 0, n);
						total += n;

						String percent = ""
								+ (int) ((total * 100) / con.getContentLength());
						state.setPercent(Integer.parseInt(percent));

					}

					in.close();
					out.close();
					state.setFinish(true);
					state.setSuccessFully(true);
					stop();
				} catch (Exception error) {
					state.setFinish(true);
					state.setSuccessFully(false);
					state.setError(error);
					stop();

				}
			}
		};

		t.start();
		return state;

	}

	public static DownloadState downloadFileSync(final String urllink,
			final String path) {
		final DownloadState state = new DownloadState();
		try {
			URL url = new URL(urllink);
			final URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			OutputStream out = new FileOutputStream(path);
			byte[] buffer = new byte[1024];
			long total = 0;
			for (int n; (n = in.read(buffer)) != -1;) {
				out.write(buffer, 0, n);
				total += n;

				String percent = ""
						+ (int) ((total * 100) / con.getContentLength());
				state.setPercent(Integer.parseInt(percent));

			}

			in.close();
			out.close();
			state.setFinish(true);
			state.setSuccessFully(true);
		} catch (Exception error) {
			state.setFinish(true);
			state.setSuccessFully(false);
			state.setError(error);

		}

		return state;

	}

}

@Data
class DownloadState {
	boolean finish = false;
	int percent = 0;
	boolean successFully = false;
	Exception error;
}
