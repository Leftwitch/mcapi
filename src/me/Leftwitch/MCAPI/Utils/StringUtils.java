package me.Leftwitch.MCAPI.Utils;

import lombok.Getter;

public class StringUtils {

	public static String removeAllUnknwonChars(String s) {
		String str = s;
		str = str.replaceAll("[^a-zA-Z]", "");
		return str;
	}

}

enum UnicodeExtras {
	BIG_BLOCK("█"), LITTLE_BLOCK("▄"), HIGH_BLOCK("▀"), RAUTE("#");
	@Getter
	String str;

	UnicodeExtras(String str) {
		this.str = str;
	}

}
