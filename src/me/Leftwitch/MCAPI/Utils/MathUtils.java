package me.Leftwitch.MCAPI.Utils;

import me.Leftwitch.MCAPI.MCAPI;

public class MathUtils {

	public static double percent(int anzhel_aller_werte, int wert) {
		return (wert * 100) / anzhel_aller_werte;
	}

	public static int random(int max) {
		return MCAPI.values.getRandom().nextInt(max);
	}

	public static int random(int from, int to) {
		return MCAPI.values.getRandom().nextInt(from - to);
	}

	public static int toInventorySize(int dynsize) {
		int newsize = dynsize;
		for (;;) {
			if (newsize % 9 == 0) {
				break;
			} else {
				newsize++;
			}
		}

		return newsize;
	}
}
