package me.Leftwitch.MCAPI.Utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.UUID;

import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Entities.NametagEntity;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;
import net.minecraft.server.v1_8_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R1.PlayerConnection;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import com.mojang.authlib.GameProfile;

public class PlayerUtils {
	private static HashMap<String, NametagEntity> nametag_entities = new HashMap();

	public static void sendToServer(Player p, String server) {
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bs);
		try {
			out.writeUTF("connect");
			out.writeUTF(server);
		} catch (IOException ex) {
		}
		p.sendPluginMessage(MCAPI.instance, "BungeeCord", bs.toByteArray());
	}

	public static void removePotions(Player p) {
		for (PotionEffect pe : p.getActivePotionEffects())
			p.removePotionEffect(pe.getType());

	}

	public static void hideNameTag(Player p) {
		NametagEntity ne;
		if (nametag_entities.containsKey(p.getName())) {
			ne = nametag_entities.get(p.getName());
		} else {
			ne = new NametagEntity(p);
			nametag_entities.put(p.getName(), ne);
		}
		ne.hideTag(p);

	}

	public static void showNameTag(Player p) {
		NametagEntity ne;
		if (nametag_entities.containsKey(p.getName())) {
			ne = nametag_entities.get(p.getName());
		} else {
			ne = new NametagEntity(p);
			nametag_entities.put(p.getName(), ne);
		}
		ne.showTag();

	}
	
	
	
	public static void sendTextOverInventory(Player p,String text){
		PacketPlayOutChat ppoc = new PacketPlayOutChat(ChatSerializer.a("{text:\""
				+ text + "\"}"),(byte) 2);
		PacketUtils.sendPacket(p, ppoc);
	}

//	public static CraftPlayer getCraftPlayer(Player p) {
//		return (CraftPlayer) p;
//	}
//
//	public static int getPing(Player p) {
//		return getCraftPlayer(p).getHandle().ping;
//
//	}
//
//	public static void sendTitle(Player p, String title, String subtitle) {
//		if(!hasRequiredVersion(p))return;
//		((CraftPlayer) p).getHandle().playerConnection
//				.sendPacket(new PacketTitle(Action.RESET));
//		if (subtitle != null && !subtitle.equalsIgnoreCase("")) {
//			IChatBaseComponent sub_title = ChatSerializer.a("{text:\""
//					+ subtitle + "\"}");
//			PacketTitle pt = new PacketTitle(Action.SUBTITLE, sub_title);
//			PacketUtils.sendPacket(p, pt);
//		}
//
//		if (title != null && !title.equalsIgnoreCase("")) {
//			IChatBaseComponent normal_title = ChatSerializer.a("{text:\""
//					+ title + "\"}");
//			PacketTitle pt = new PacketTitle(Action.TITLE, normal_title);
//			PacketUtils.sendPacket(p, pt);
//		}
//
//	}
//
//	public static void sendTitle(Player p, String title, String subtitle,
//			int fadein, int stay, int fadeout) {
//		if(!hasRequiredVersion(p))return;
//		((CraftPlayer) p).getHandle().playerConnection
//				.sendPacket(new PacketTitle(Action.RESET));
//
//		PacketTitle pttimes = new PacketTitle(Action.TIMES, fadein, stay,
//				fadeout);
//		PacketUtils.sendPacket(p, pttimes);
//		if (subtitle != null && !subtitle.equalsIgnoreCase("")) {
//			IChatBaseComponent sub_title = ChatSerializer.a("{text:\""
//					+ subtitle + "\"}");
//			PacketTitle pt = new PacketTitle(Action.SUBTITLE, sub_title);
//			PacketUtils.sendPacket(p, pt);
//		}
//
//		if (title != null && !title.equalsIgnoreCase("")) {
//			IChatBaseComponent normal_title = ChatSerializer.a("{text:\""
//					+ title + "\"}");
//			PacketTitle pt = new PacketTitle(Action.TITLE, normal_title);
//			PacketUtils.sendPacket(p, pt);
//		}
//
//	}
	
	
	public static void sendTabHeader(Player p,String headerText,String footerText){
		if(!hasRequiredVersion(p))return;
		setPlayerListHeader(p, headerText, footerText);

	}

	
	public static void addPlayerToTabList(Player p,GameProfile prof,int ping){
        PacketPlayOutPlayerInfo ppop = new PacketPlayOutPlayerInfo();
        
        try {
        	ReflectionUtil.setValue(ppop, "action", 0);
			ReflectionUtil.setValue(ppop, "username", prof.getName());
			ReflectionUtil.setValue(ppop, "player", (prof == null ? new GameProfile(UUID.randomUUID(), "") : prof));
			ReflectionUtil.setValue(ppop, "ping", (ping == -1 ? 2000 : ping));
			ReflectionUtil.setValue(ppop, "gamemode", 0);
			PacketUtils.sendPacket(p, ppop);

        } catch (Exception err) {
			// TODO Auto-generated catch block
			err.printStackTrace();
		}
	}
	
	public static void removePlayerFromTabList(Player p,GameProfile prof,int ping){
		if(!hasRequiredVersion(p))return;
        PacketPlayOutPlayerInfo ppop = new PacketPlayOutPlayerInfo();
        
        try {
        	ReflectionUtil.setValue(ppop, "action", 4);
			ReflectionUtil.setValue(ppop, "username", prof.getName());
			ReflectionUtil.setValue(ppop, "player", (prof == null ? new GameProfile(UUID.randomUUID(), "") : prof));
			PacketUtils.sendPacket(p, ppop);

        } catch (Exception err) {
			// TODO Auto-generated catch block
			err.printStackTrace();
		}
	}
	
	public static void sendActionBarText(Player player,String text){
		if(!hasRequiredVersion(player))return;
		try{
	        CraftPlayer p = (CraftPlayer) player;
	        IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + text + "\"}");
	        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte) 2);
	      
	        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
		} catch (Exception error){
			System.out.println("Version nich Kompatibel f�r  \"sendActionBarText(Player arg0,String arg1);\"");
		}
	    
	}
	
	public static boolean hasRequiredVersion(Player arg0) {
		return true;
	}
	
	
	public static void setPlayerListHeader(Player player,String header,String footer){
        CraftPlayer cplayer = (CraftPlayer) player;
        PlayerConnection connection = cplayer.getHandle().playerConnection;
        IChatBaseComponent hj = ChatSerializer.a("{'text':'"+header+"'}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try{
            Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, hj);
            headerField.setAccessible(!headerField.isAccessible());
          
        } catch (Exception e){
            e.printStackTrace();
        }
        connection.sendPacket(packet);
    }
  
  
    public static void setPlayerListFooter(Player player,String footer){
        CraftPlayer cp = (CraftPlayer) player;
        PlayerConnection con = cp.getHandle().playerConnection;
        IChatBaseComponent fj = ChatSerializer.a("{'text':'"+footer+"'}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try{
            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, fj);
            footerField.setAccessible(!footerField.isAccessible());
        }catch(Exception e){
            e.printStackTrace();
        }
        con.sendPacket(packet);
    }
 
}
