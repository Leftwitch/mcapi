package me.Leftwitch.MCAPI.Utils;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

public class InventoryUtils {

	
	
	public static ItemStack rename(ItemStack is,String name){
		ItemStack stack = is;
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		stack.setItemMeta(meta);
		return stack;
	}
	
	public static ItemStack setLore(ItemStack is,String... lore){
		ItemStack stack = is;
		ItemMeta meta = stack.getItemMeta();
		meta.setLore(Lists.newArrayList(lore));
		stack.setItemMeta(meta);
		return stack;
	}
}
