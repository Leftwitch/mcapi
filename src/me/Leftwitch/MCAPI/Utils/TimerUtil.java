package me.Leftwitch.MCAPI.Utils;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.bukkit.Bukkit;

import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Annotations.AsyncTimer;
import me.Leftwitch.MCAPI.Annotations.SyncTimer;
import me.Leftwitch.MCAPI.Interfaces.TimerInterface;

public class TimerUtil {
	public static HashMap<AsyncTimer, Long> vergangeneZeitAsync = new HashMap<>();
	public static HashMap<SyncTimer, Long> vergangeneZeitSync = new HashMap<>();

	public static void checkIntervalsAsync() {
		for (TimerInterface ti : MCAPI.values.getTimerInterfaces()) {
			try {
				Method[] methods = ti.getClass().getMethods();

				for (int i = 0; i < methods.length; i++) {
					AsyncTimer asyncTimers = methods[i]
							.getAnnotation(AsyncTimer.class);
					if (asyncTimers != null) {
						int interval = asyncTimers.intervalInTicks() * 50;
						Long curTime = System.currentTimeMillis();
						if (!vergangeneZeitAsync.containsKey(asyncTimers)) {
							vergangeneZeitAsync.put(asyncTimers, curTime);
						} else {
							int oldTime = (int) (long) vergangeneZeitAsync
									.get(asyncTimers);
							int over = (int) (curTime - oldTime);
							if (over >= interval) {
								methods[i].invoke(ti.getClass().newInstance());
								vergangeneZeitAsync.put(asyncTimers, curTime);
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void checkIntervalsSync() {
		for (TimerInterface ti : MCAPI.values.getTimerInterfaces()) {
			try {
				Method[] methods = ti.getClass().getMethods();

				for (int i = 0; i < methods.length; i++) {
					SyncTimer SyncTimers = methods[i]
							.getAnnotation(SyncTimer.class);
					if (SyncTimers != null) {
						int interval = SyncTimers.intervalInTicks() * 50;
						Long curTime = System.currentTimeMillis();
						if (!vergangeneZeitSync.containsKey(SyncTimers)) {
							vergangeneZeitSync.put(SyncTimers, curTime);
						} else {
							int oldTime = (int) (long) vergangeneZeitSync
									.get(SyncTimers);
							int over = (int) (curTime - oldTime);
							if (over >= interval) {
								methods[i].invoke(ti.getClass().newInstance());
								vergangeneZeitSync.put(SyncTimers, curTime);
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void startSchedulers() {
		Bukkit.getScheduler().runTaskTimer(MCAPI.instance, new Runnable() {

			@Override
			public void run() {
				checkIntervalsSync();

			}
		}, 0, 1);

		Bukkit.getScheduler().runTaskTimerAsynchronously(MCAPI.instance,
				new Runnable() {

					@Override
					public void run() {
						checkIntervalsAsync();

					}
				}, 0, 1);
	}

}
