package me.Leftwitch.MCAPI.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import lombok.Getter;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class MapGrenze {
	  @Getter
	  public  ArrayList<Location> grenzen = new ArrayList<>();
	
	  public MapGrenze(final Location pos, double radiusX,  double radiusY,  double radiusZ, final boolean filled) {
	        int affected2 = 0;
	        radiusX += 0.5;
  	        radiusY += 0.5;
  	        radiusZ += 0.5;
  	        final HashMap<Integer, Double> hmap = new HashMap<>();
  	        hmap.put(1, radiusX);
  	        hmap.put(2, radiusY);
  	        hmap.put(3, radiusZ);

	      Thread t = new Thread(){
	    	  public void run(){
	  	       try{
	  	    	 int affected = 0;
		  	        double radiusX2 = hmap.get(1);
		  	        double radiusY2 = hmap.get(2);
		  	        double radiusZ2 = hmap.get(3);

		  	        final double invRadiusX = 1 / radiusX2;
		  	        final double invRadiusY = 1 / radiusY2;
		  	        final double invRadiusZ = 1 / radiusZ2;

		  	        final int ceilRadiusX = (int) Math.ceil(radiusX2);
		  	        final int ceilRadiusY = (int) Math.ceil(radiusY2);
		  	        final int ceilRadiusZ = (int) Math.ceil(radiusZ2);

		  	        double nextXn = 0;
		  	        forX: for (int x = 0; x <= ceilRadiusX; ++x) {
		  	            final double xn = nextXn;
		  	            nextXn = (x + 1) * invRadiusX;
		  	            double nextYn = 0;
		  	            forY: for (int y = 0; y <= ceilRadiusY; ++y) {
		  	                final double yn = nextYn;
		  	                nextYn = (y + 1) * invRadiusY;
		  	                double nextZn = 0;
		  	                forZ: for (int z = 0; z <= ceilRadiusZ; ++z) {
		  	                    final double zn = nextZn;
		  	                    nextZn = (z + 1) * invRadiusZ;

		  	                    double distanceSq = lengthSq(xn, yn, zn);
		  	                    if (distanceSq > 1) {
		  	                        if (z == 0) {
		  	                            if (y == 0) {
		  	                                break forX;
		  	                            }
		  	                            break forY;
		  	                        }
		  	                        break forZ;
		  	                    }

		  	                    if (!filled) {
		  	                        if (lengthSq(nextXn, yn, zn) <= 1 && lengthSq(xn, nextYn, zn) <= 1 && lengthSq(xn, yn, nextZn) <= 1) {
		  	                            continue;
		  	                        }
		  	                    }

		  	                    if (setBlock(pos.add(x, y, z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(-x, y, z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(x, -y, z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(x, y, -z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(-x, -y, z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(x, -y, -z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(-x, y, -z))) {
		  	                        ++affected;
		  	                    }
		  	                    if (setBlock(pos.add(-x, -y, -z))) {
		  	                        ++affected;
		  	                    }
		  	                }
		  	            }
		  	        }
		    	     
	  	       } catch (Exception error){
	  	    	   
	  	       }
	    	  }
	      };
	      t.start();

	    }
	  
	  
	  private final double lengthSq(double x, double y, double z) {
	        return (x * x) + (y * y) + (z * z);
	    }

	    private final double lengthSq(double x, double z) {
	        return (x * x) + (z * z);
	    }
	    public boolean setBlock(Location pt) {
	    	 grenzen.add(new Location(Bukkit.getWorld("ender"), pt.getX(), pt.getY(), pt.getZ()));
	         return true;
	    }
	   
	    public static MapGrenze createGrenze(Location mitte,int radius){
	    	Double radiusX,radiusY,radiusZ;
            radiusX = radiusY = radiusZ = Math.max(1, (double)radius);
				return new MapGrenze(mitte, radius,radius,radius,false);

	    }
	  
}
