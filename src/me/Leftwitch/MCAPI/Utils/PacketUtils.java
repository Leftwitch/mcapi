package me.Leftwitch.MCAPI.Utils;

import net.minecraft.server.v1_8_R1.Packet;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketUtils {
	// --- Packets Senden --- /
	public static void sendPacket(Player p, Packet... packet) {
		CraftPlayer cp = (CraftPlayer) p;
		for (Packet pack : packet)
			cp.getHandle().playerConnection.sendPacket(pack);
	}
	
	
}
