package me.Leftwitch.MCAPI.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL {

	public static Connection c;

	public MySQL(String host,String user,String password,String database,int port) {
		try {
			Class.forName("org.mysql.JDBC");
			c = DriverManager.getConnection("jdbc:mysql://" + host + ":"+port+"/" + database,user, password); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
	}

	public static void disconnect() throws SQLException {
		Connection conn = c;
		conn.close();
	}


	public void createTable(String name, String... spalten) {
		try {
			String spaltenstr = "";
			for (String s : spalten)
				spaltenstr += s + ",";
			spaltenstr = spaltenstr.substring(0, spaltenstr.length() - 1);
			Statement sampleQueryStatement = c.createStatement();

			sampleQueryStatement.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ name + "(" + spalten + ")");
			sampleQueryStatement.close();

		} catch (Exception err) {
			err.printStackTrace();
		}

	}

	public void insertInTable(String tablename, String... values) {
		try {
			String spaltenstr = "";
			for (String s : values)
				spaltenstr += "'" + s + "',";
			spaltenstr = spaltenstr.substring(0, spaltenstr.length() - 1);
			Statement sampleQueryStatement = c.createStatement();

			sampleQueryStatement.executeUpdate("INSERT INTO " + tablename
					+ " VALUES (" + spaltenstr + ")");
			sampleQueryStatement.close();

		} catch (Exception err) {
			err.printStackTrace();
		}

	}

	public static void Update(String qry) {
		try {
			Statement stmt = c.createStatement();
			stmt.executeUpdate(qry);

			stmt.close();
		} catch (Exception ex) {

			System.err.println(ex);
		}
	}

	public static ResultSet Query(String qry) {
		ResultSet rs = null;
		try {
			Statement stmt = c.createStatement();
			rs = stmt.executeQuery(qry);
		} catch (Exception ex) {
			System.err.println(ex);
		}

		return rs;
	}
	
	
	public static void switchDatabase(String database){
		Query("use "+database);
	}

}