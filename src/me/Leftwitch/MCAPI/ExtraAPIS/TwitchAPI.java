package me.Leftwitch.MCAPI.ExtraAPIS;

import lombok.Data;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Utils.JsonUtils;

import org.bukkit.craftbukkit.libs.com.google.gson.JsonArray;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;



public class TwitchAPI {
	public static Twitch_Stream getStream(String channelname) {
		try {
			String json = JsonUtils.readJsonFromUrl("http://api.justin.tv/api/stream/list.json?channel="
							+ channelname);

			Twitch_Stream stream = new Twitch_Stream();
			if (json.equalsIgnoreCase("[]")) {
				stream.setOnline(false);
				return stream;
			}
			JsonArray jb = MCAPI.values.getGson().fromJson(json, JsonArray.class);
			JsonObject jo = (JsonObject) jb.get(0);
			stream.setOnline(true);
			stream.load(jo);
			return stream;
		} catch (Exception error) {
			error.printStackTrace();
		}

		return null;

	}
}
@Data
 class Twitch_Stream {

	boolean online;

	int broadcast_part;

	boolean featured;

	boolean channel_subscription;

	String id;

	String category;

	String title;

	int channel_count;

	int video_height;

	int site_count;

	boolean embed_enabled;

	String up_time;

	String meta_game;

	String format;

	int embed_count;

	String stream_type;

	boolean abuse_reported;

	int video_width;

	String geo;

	String name;

	String language;

	int stream_count;

	double video_bitrate;

	String broadcaster;

	int channel_view_count;

	// Other Infos

	String username;

	String status;

	String channel_url;

	boolean producer;

	String subcategory_title;

	String screen_cap_url_large;

	String screen_cap_url_small;

	String screen_cap_url_medium;

	String screen_cap_url_huge;

	String timezone;

	String category_title;

	int views_count;

	public void load(JsonObject job) {
		setBroadcast_part(job.get("broadcast_part").getAsInt());
		setFeatured(job.get("featured").getAsBoolean());
		setChannel_subscription(job.get("channel_subscription").getAsBoolean());
		setId(job.get("id").getAsString());
		setCategory(job.get("category").getAsString());
		setTitle(job.get("title").getAsString());
		setChannel_count(job.get("channel_count").getAsInt());
		setVideo_height(job.get("video_height").getAsInt());
		setSite_count(job.get("site_count").getAsInt());
		setEmbed_enabled(job.get("embed_enabled").getAsBoolean());
		setUp_time(job.get("up_time").getAsString());
		setMeta_game(job.get("meta_game").getAsString());
		setFormat(job.get("format").getAsString());
		setEmbed_count(job.get("embed_count").getAsInt());
		setStream_type(job.get("stream_type").getAsString());
		setAbuse_reported(job.get("abuse_reported").getAsBoolean());
		setVideo_width(job.get("video_width").getAsInt());
		setGeo(job.get("geo").getAsString());
		setName(job.get("name").getAsString());
		setLanguage(job.get("language").getAsString());
		setStream_count(job.get("stream_count").getAsInt());
		setVideo_bitrate(job.get("video_bitrate").getAsDouble());
		setBroadcaster(job.get("broadcaster").getAsString());
		setChannel_view_count(job.get("channel_view_count").getAsInt());
		// Other Infos
		setUsername(job.get("channel").getAsJsonObject().get("login")
				.getAsString());
		setTitle(job.get("channel").getAsJsonObject().get("status")
				.getAsString());
		setChannel_url(job.get("channel").getAsJsonObject().get("channel_url")
				.getAsString());
		setProducer(job.get("channel").getAsJsonObject().get("producer")
				.getAsBoolean());
		// setTags(job.get("channel").getAsJsonObject().get("tags").getAsString());
		setSubcategory_title(job.get("channel").getAsJsonObject()
				.get("subcategory_title").getAsString());
		setScreen_cap_url_large(job.get("channel").getAsJsonObject()
				.get("screen_cap_url_large").getAsString());
		setScreen_cap_url_small(job.get("channel").getAsJsonObject()
				.get("screen_cap_url_small").getAsString());
		setScreen_cap_url_medium(job.get("channel").getAsJsonObject()
				.get("screen_cap_url_medium").getAsString());
		setScreen_cap_url_huge(job.get("channel").getAsJsonObject()
				.get("screen_cap_url_huge").getAsString());
		setTimezone(job.get("channel").getAsJsonObject().get("timezone")
				.getAsString());
		setCategory_title(job.get("channel").getAsJsonObject()
				.get("category_title").getAsString());
		setViews_count(job.get("channel").getAsJsonObject().get("views_count")
				.getAsInt());

	}

}