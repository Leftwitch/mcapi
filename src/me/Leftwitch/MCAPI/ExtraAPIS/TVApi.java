package me.Leftwitch.MCAPI.ExtraAPIS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.Leftwitch.MCAPI.DataClasses.TVChannelInfo;

public class TVApi {
	



public static List<TVChannelInfo> getTVChannelInfos(String source){
	Matcher matcher = Pattern.compile("<title>(.*?)</title>").matcher(source);
	List<TVChannelInfo> infos = new ArrayList<TVChannelInfo>();
	while (matcher.find()) {
		String found = matcher.group();
	    if(found.contains("|")){
	    	found=found.replace("<title>", "");
	    	found=found.replace("</title>", "");
	    	found=found.replace(" | ", "-##-");
	    	String[] chinfo = found.split("-##-");
	    	String time = chinfo[0];
	    	String channelname = chinfo[1];
	    	String current = chinfo[2];
	    	TVChannelInfo tci = new TVChannelInfo(channelname, current, time);
	    	infos.add(tci);
	    } else {
	    	continue;
	    }
	}
	
	return infos;
}

public static String readContextFromUrl(String urlString) throws Exception {
	BufferedReader reader = null;
	try {
		URL url = new URL(urlString);
		reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer buffer = new StringBuffer();
		int read;
		char[] chars = new char[1024];
		while ((read = reader.read(chars)) != -1)
			buffer.append(chars, 0, read);
		

		return buffer.toString();
	} finally {
		if (reader != null)
			reader.close();
	}
}
}



