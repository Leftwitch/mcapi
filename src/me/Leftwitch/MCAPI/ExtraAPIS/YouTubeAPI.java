package me.Leftwitch.MCAPI.ExtraAPIS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Utils.JsonUtils;

import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonArray;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;

public class YouTubeAPI {

	public static Gson gson = MCAPI.values.getGson();

	public static String readJsonFromUrl(String urlString) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	public static String getGooglePlusId(String uri) throws Exception {
		String endid = uri.split("/")[uri.split("/").length - 1];
		String json = JsonUtils.readJsonFromUrl("http://gdata.youtube.com/feeds/api/users/"
						+ endid + "?alt=json");
		JsonObject jo = gson.fromJson(json, JsonObject.class);
		String gplusid = jo.get("entry").getAsJsonObject()
				.get("yt$googlePlusUserId").getAsJsonObject().get("$t")
				.getAsString();
		return gplusid;
	}

	public static String getChannelFromGPlusId(String plusid, String uri,
			String developerkey) throws Exception {
	
		String endid = uri;

		try {
			String json = JsonUtils.readJsonFromUrl("https://www.googleapis.com/plus/v1/people/"
							+ plusid + "?key=" + developerkey);
			JsonObject jb = gson.fromJson(json, JsonObject.class);
			JsonObject jo = jb;
			JsonArray ja = jo.get("urls").getAsJsonArray();
			for (int i = 0; i < ja.size(); i++) {
				JsonObject jsob = (JsonObject) ja.get(i);
				if (jsob.get("value").getAsString().toLowerCase()
						.contains("youtube")) {
					return jsob.get("value").getAsString();
				}
			}
			return "No Google Plus Data To Find The Channel";
		} catch (NullPointerException e) {
			e.printStackTrace();
			return "No Google Plus Data To Find The Channel";
		}

	}

	public static YoutubeChannel getYTChannelFromUri(String uri) {
		try {
			System.out.println(uri);
			YoutubeChannel ch;
			String json = readJsonFromUrl(uri + "?alt=json");
			JsonObject jb = gson.fromJson(json, JsonObject.class);
			String username = jb.get("entry").getAsJsonObject()
					.get("yt$username").getAsJsonObject().get("$t")
					.getAsString();
			String about_us = jb.get("entry").getAsJsonObject().get("content")
					.getAsJsonObject().get("$t").getAsString();
			String title = jb.get("entry").getAsJsonObject().get("title")
					.getAsJsonObject().get("$t").getAsString();
			String subscribers = jb.get("entry").getAsJsonObject()
					.get("yt$statistics").getAsJsonObject()
					.get("subscriberCount").getAsString();
			String gplusid = jb.get("entry").getAsJsonObject()
					.get("yt$googlePlusUserId").getAsJsonObject().get("$t")
					.getAsString();
			String firstname = "";
			String lastname = "";
			if (jb.get("entry").getAsJsonObject().get("yt$firstName") != null) {
				firstname = jb.get("entry").getAsJsonObject()
						.get("yt$firstName").getAsJsonObject().get("$t")
						.getAsString();
			}
			if (jb.get("entry").getAsJsonObject().get("yt$lastName") != null) {

				lastname = jb.get("entry").getAsJsonObject().get("yt$lastName")
						.getAsJsonObject().get("$t").getAsString();
				;
			}
			String location = jb.get("entry").getAsJsonObject()
					.get("yt$location").getAsJsonObject().get("$t")
					.getAsString();
			;
			String lastwebaccess = jb.get("entry").getAsJsonObject()
					.get("yt$statistics").getAsJsonObject()
					.get("lastWebAccess").getAsString();
			int videowatchcount = jb.get("entry").getAsJsonObject()
					.get("yt$statistics").getAsJsonObject()
					.get("videoWatchCount").getAsInt();
			int viewcount = jb.get("entry").getAsJsonObject()
					.get("yt$statistics").getAsJsonObject().get("viewCount")
					.getAsInt();
			String totaluploadviews = jb.get("entry").getAsJsonObject()
					.get("yt$statistics").getAsJsonObject()
					.get("totalUploadViews").getAsString();
			String avatar = jb.get("entry").getAsJsonObject()
					.get("media$thumbnail").getAsJsonObject().get("url")
					.getAsString();
			ch = new YoutubeChannel(about_us, title, username, subscribers,
					gplusid, firstname, lastname, location, lastwebaccess,
					videowatchcount, viewcount, totaluploadviews, avatar);
			return ch;
		} catch (Exception err) {
			err.printStackTrace();
			return null;
		}
	}
	
	
	
	
	public static YoutubeChannel getYTChannelFromName(String channel){
		return getYTChannelFromUri("http://gdata.youtube.com/feeds/api/users/"+channel);
	}

}


@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Data
class YoutubeChannel {
     String about_us;
     String title;
     String username;
     String subscribers; 
	 String googleplusid;
	 
	 String firstname = "";
	 String lastname = "";
	 String location;
	 String lastwebaccess;
	 
	 int videoWatchCount;
	 int viewcount;
	 String totaluploadViews;
	 String avatar;
	
}

