package me.Leftwitch.MCAPI.ServerPing;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Data
public class ServerPingResponse {
Players players;
String motd;
boolean online;
int ping;
String hostname;
String version;
byte protocol;
String favicon;
}

