package me.Leftwitch.MCAPI.ServerPing;


import java.util.Map;

import me.Leftwitch.MCAPI.Utils.JsonUtils;

import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;

public class ServerPing {
	
	// --- Ping --- /
	public static ServerPingResponse ping(String ip,int port) throws Exception{
		ServerPingResponse spr = new ServerPingResponse(null, null, false, -1, "127.0.0.1", "1.0",(byte) 1, "");
	    Gson gson = new Gson();
		// --- Infos Abrufen --- /
	    String jsonstr = JsonUtils.readJsonFromUrl("http://mcapi.sweetcode.de/api/v2/?ip="+ip+"&port="+port);
        TestInfo ti = gson.fromJson(jsonstr, TestInfo.class);

		// --- Values Setzen --- /
        spr.setOnline(ti.status);
        spr.setHostname(ti.hostname);
        spr.setVersion(ti.version);
        spr.setProtocol((byte)ti.protocol);
		Players players = new Players(ti.player.get("currently"), ti.player.get("max"));
		spr.setPlayers(players);
		spr.setMotd(((String)ti.list.get("motd_raw")).replace("\\u00a7", "�"));
		spr.setFavicon((String)ti.list.get("favicon"));
		spr.setPing((int)(double)ti.list.get("ping"));
		return spr;
	}
	
	static class TestInfo {
		String hostname;
		boolean status;
		String version;
		int protocol;
		Map<String,Integer> player;
		Map<String,Object> list;

	}
}
