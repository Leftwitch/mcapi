package me.Leftwitch.MCAPI.ServerPing;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Data
public class Players{
	int online;
	int max;
}