package me.Leftwitch.MCAPI.Minigame;

import java.util.List;

import lombok.Data;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.BungeeCord.ServerShutdownScheduler;
import net.minecraft.server.v1_8_R1.MinecraftServer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.google.common.collect.Lists;

@Data
public class Minigame {
	int lobbyCountDown = 120;
	int minPlayers = 4;
	int maxPlayers = 24;
	String name = "Unknown Game";
	String prefix = "Unknown Prefix";
	boolean autoSpawnOnLobby = true;
	boolean build = false;
	boolean autoRespawn = false;
	boolean spectatorAfterDeath = false;
	boolean food = false;
	Location lobby;
	boolean joinIngame = false;
    List<Player> spectators = Lists.newArrayList();
    List<Player> playersIngame = Lists.newArrayList();
    String joinMessage = "�9[PLAYER] �6Hat den server betreten";
    String leaveMessage = "�9[PLAYER] �6Hat den server verlassen";
    boolean ingame = false;
    boolean dropItemsOnDeath = false;
	Scoreboard scoreboard;
	Team spectatorTeam;
	boolean restarting = false;
	ServerShutdownScheduler shutdownScheduler = null;
	
	public void startLobbyCountdown(boolean sync) {
		CountdownRunnable cr = new CountdownRunnable(lobbyCountDown,
				"�6Das Spiel startet in �5[TIME] �6Sekunden", prefix, sync);
		cr.setMinplayers(minPlayers);
		cr.start();
	}

	public void load() {
		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		spectatorTeam=scoreboard.registerNewTeam("Spectator");
		spectatorTeam.setCanSeeFriendlyInvisibles(true);
		spectatorTeam.setAllowFriendlyFire(false);
		spectatorTeam.setPrefix("�6Spectator �7");
		spectatorTeam.setDisplayName("Spectator");
		loadListener(new DefaultMinigameListener(this));
		MinecraftServer.getServer().setAllowFlight(true);
	}

	public void loadListener(Listener list) {
		Bukkit.getPluginManager().registerEvents(list, MCAPI.instance);
	}
	
	
	
	
	public void restart(String lobbyserver){
		restarting=true;
		ServerShutdownScheduler scheduler = new ServerShutdownScheduler();
		scheduler.setMaxtrys(30);
		scheduler.setTargetServer(lobbyserver);
		scheduler.start();
		shutdownScheduler=scheduler;
	}
	
	
	public void cancelRestart(){
		shutdownScheduler.cancel();
		restarting=false;
	}
}
