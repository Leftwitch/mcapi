package me.Leftwitch.MCAPI.Minigame;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import lombok.Data;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Events.CountdownEndEvent;

@Data
public class CountdownRunnable implements Runnable {
	BukkitTask task;
	int time = 120;
	int starttime = 120;
	String bcastMessage = "";
	String prefix;
	boolean sync = false;
	int minplayers = 4;

	@Override
	public void run() {
		if (time <= 0)
			return;
		time--;
		String s = time + "";
		if (s.endsWith("0") || time <= 10) {
			Bukkit.broadcastMessage(prefix + " "
					+ bcastMessage.replace("[TIME]", "" + time));

			if (time == 0) {
				if (Bukkit.getOnlinePlayers().length >= minplayers) {
					Bukkit.getPluginManager().callEvent(
							new CountdownEndEvent(this));
				} else {
					Bukkit.broadcastMessage(prefix
							+ " �6Zu wenig spieler Online ben�tigt werden �3"
							+ minplayers);
					reset();
				}
			}

		}

	}

	public void reset() {
		cancel();
		start();
	}

	public void start() {
		time = starttime;
		if (!sync)
			task = Bukkit.getScheduler().runTaskTimerAsynchronously(
					MCAPI.instance, this, 20, 20);
		else
			task = Bukkit.getScheduler().runTaskTimer(MCAPI.instance, this, 20,
					20);
	}

	public void cancel() {
		if (task != null) {
			task.cancel();
			time = starttime;
		} else {
			System.out.println("Task Never Started!");
		}
	}

	public CountdownRunnable(int time, String message, String prefix,
			boolean sync) {
		this.time = time;
		this.starttime = time;
		this.bcastMessage = message;
		this.prefix = prefix;
		this.sync = sync;
	}

}
