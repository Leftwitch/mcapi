package me.Leftwitch.MCAPI.Minigame;

import lombok.AllArgsConstructor;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Utils.PlayerUtils;
import net.minecraft.server.v1_8_R1.EnumClientCommand;
import net.minecraft.server.v1_8_R1.PacketPlayInClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@AllArgsConstructor
public class DefaultMinigameListener implements Listener {
	Minigame game;

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBuild(BlockPlaceEvent e) {
		if (e.getPlayer().isOp())
			return;
		if (game.isBuild())
			return;
		e.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBuild(BlockBreakEvent e) {
		if (e.getPlayer().isOp())
			return;
		if (game.isBuild())
			return;
		e.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onFoodChange(FoodLevelChangeEvent e) {
		if (game.isFood())
			return;
		e.setFoodLevel(20);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(PlayerJoinEvent e) {
		e.setJoinMessage(game.getJoinMessage().replace("[PLAYER]",
				e.getPlayer().getName()));
		if (game.isAutoSpawnOnLobby())
			e.getPlayer().teleport(game.lobby);
		e.getPlayer().resetMaxHealth();
		e.getPlayer().setHealth(20.0);
		e.getPlayer().setFoodLevel(20);
		e.getPlayer().setLevel(0);
		e.getPlayer().setAllowFlight(false);
		e.getPlayer().setFlying(false);
		e.getPlayer().setFlySpeed(0.2F);
		e.getPlayer().setWalkSpeed(0.2F);
		PlayerUtils.removePotions(e.getPlayer());

	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		e.setQuitMessage(game.getLeaveMessage().replace("[PLAYER]",
				e.getPlayer().getName()));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager().getType() != EntityType.PLAYER)
			return;
		Player p = (Player) e.getDamager();
		if (game.spectators.contains(p)) {
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntity().getType() != EntityType.PLAYER)
			return;
		Player p = (Player) e.getEntity();
		if (game.spectators.contains(p)) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		if (game.isRestarting()) {
			e.disallow(Result.KICK_OTHER, "�4Der Server Restartet Gerade");
			return;
		}
		if (game.ingame) {
			if (game.isJoinIngame()) {
				toSpectator(e.getPlayer());
				e.allow();
				return;
			} else {
				e.disallow(Result.KICK_OTHER, "�4Dieser Server Ist InGame!");

			}
			return;
		}
		if (e.getPlayer().isOp()
				|| Bukkit.getOnlinePlayers().length < game.maxPlayers)
			e.allow();
		else
			e.disallow(Result.KICK_OTHER, "�4Dieser Server Ist Voll!");

	}

	@EventHandler
	public void onDie(final PlayerDeathEvent e) {
		if (game.isAutoRespawn()) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(MCAPI.instance,
					new Runnable() {
						@Override
						public void run() {
							PacketPlayInClientCommand packet = new PacketPlayInClientCommand(
									EnumClientCommand.PERFORM_RESPAWN);
							((CraftPlayer) e.getEntity()).getHandle().playerConnection
									.a(packet);
						}
					}, 2L);

		}

		if (game.isSpectatorAfterDeath()) {
			toSpectator(e.getEntity());
		}

		if (!game.dropItemsOnDeath) {
			e.getDrops().clear();
		}
	}

	@EventHandler
	public void onKick(PlayerKickEvent e) {
		if (game.isRestarting()) {
			e.setCancelled(true);
		}
	}

	public void toSpectator(final Player p) {
		p.sendMessage(game.prefix + " �6Du bist nun ein Spectator!");
		game.spectators.add(p);
		for (Player ps : Bukkit.getOnlinePlayers()) {
			if (!game.spectators.contains(ps)) {
				ps.hidePlayer(p);
			} else {
				ps.showPlayer(p);
				p.showPlayer(ps);
			}
		}
		game.spectatorTeam.addPlayer(p);
		p.setScoreboard(game.scoreboard);
		p.getInventory().clear();
		p.setFoodLevel(20);
		p.setAllowFlight(true);
		p.setFlying(true);
		p.setFlySpeed(0.3F);
		p.setWalkSpeed(0.3F);
		p.setMaxHealth(2.0);
		p.setHealth(2.0);
		p.updateInventory();
		p.setGameMode(GameMode.ADVENTURE);
		Bukkit.getScheduler().runTaskLater(MCAPI.instance, new Runnable() {

			@Override
			public void run() {
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.INVISIBILITY, 9999999, 99));

			}
		}, 10);

	}
}
