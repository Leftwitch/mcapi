package me.Leftwitch.MCAPI;

import me.Leftwitch.MCAPI.Commands.CommandMCAPI;
import me.Leftwitch.MCAPI.Interfaces.TimerInterface;
import me.Leftwitch.MCAPI.Listeners.ForceFieldListener;
import me.Leftwitch.MCAPI.Map.RenderThread;
import me.Leftwitch.MCAPI.Utils.ForceFieldUtil;
import me.Leftwitch.MCAPI.Utils.TimerUtil;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class MCAPI extends JavaPlugin {
	public static Values values = new Values();
	public static MCAPI instance;

	@Override
	public void onEnable() {
		System.out.println("--------------------------------------");
		System.out.println("Lade MC-API...");
		instance = this;
		values.renderThread = new RenderThread();
		values.renderThread.startThread();
		System.out.println("Starte Map - Image Thread");
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		System.out.println("Registriere Befehle und Events");
        Bukkit.getPluginManager().registerEvents(new ForceFieldListener(), this);
		getCommand("mcapi").setExecutor(new CommandMCAPI());
		System.out.println("Starte Scheduler");
		ForceFieldUtil.startForcefieldScheduler();
		TimerUtil.startSchedulers();
		System.out.println("Fertig!");
		System.out.println("--------------------------------------");
	}
	
	
	
	
	public static void registerTimerInterface(TimerInterface ti){
		values.getTimerInterfaces().add(ti);
	}
}
