package me.Leftwitch.MCAPI;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import lombok.Data;
import me.Leftwitch.MCAPI.DataClasses.ForceField;
import me.Leftwitch.MCAPI.Interfaces.TimerInterface;
import me.Leftwitch.MCAPI.Map.RenderThread;

import org.bukkit.craftbukkit.libs.com.google.gson.Gson;

import com.google.common.collect.Lists;

@Data
public class Values {
	ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    RenderThread renderThread;
    Gson gson = new Gson();
    Random random = new Random();
    List<ForceField> activeForceFields = Lists.newArrayList();
    List<TimerInterface> timerInterfaces = Lists.newArrayList();
    
}
