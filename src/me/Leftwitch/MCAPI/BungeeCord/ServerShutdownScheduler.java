package me.Leftwitch.MCAPI.BungeeCord;

import lombok.Getter;
import lombok.Setter;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Utils.PlayerUtils;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

public class ServerShutdownScheduler implements Runnable {
	@Getter
	@Setter
	int trys = 0;
	@Getter
	@Setter
	int maxtrys = 20;
	BukkitTask task;
	@Getter
	@Setter
    String targetServer = "lobby";
    
	@Override
	public void run() {
		if (Bukkit.getOnlinePlayers().length == 0  || trys==maxtrys) {
			Bukkit.broadcastMessage("�3|> �6�oServer Restartet Jetzt �3<|");
//			for (Player ps : Bukkit.getOnlinePlayers())PlayerUtils.sendTitle(ps, "�4�nVerbindungsfehler :(", "�eDu wirst nun gekickt Werden");
			cancel();
			Bukkit.getScheduler().runTaskLater(MCAPI.instance, new Runnable() {
				
				@Override
				public void run() {
					Bukkit.shutdown();					
				}
			}, 20*3);
			return;
		}
		trys++;
		for (Player ps : Bukkit.getOnlinePlayers()) {
			for(int i=0;i<80;i++)ps.sendMessage("");
		 ps.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 9999999, 0));
		 ps.playSound(ps.getLocation(), Sound.ORB_PICKUP, 100, -100);
		 ps.closeInventory();
		 ps.sendMessage("�3|> �6Verbinde Zum Server �7"+targetServer+" �6Versuch: �5"+trys+"�7/�5"+maxtrys);
		 PlayerUtils.sendTabHeader(ps, "�3Server Startet Neu", "�6Versuch: �5"+trys+"�7/�5"+maxtrys);
//		 PlayerUtils.sendTitle(ps, "�6�nVerbindungsaufbau...", "�6Verbinde Zum Server �7"+targetServer+" �6Versuch: �5"+trys+"�7/�5"+maxtrys);
         ps.setVelocity(new Vector(0,10,0));
		 PlayerUtils.sendToServer(ps, targetServer);
		}

	}

	public void start() {
//		for (Player ps : Bukkit.getOnlinePlayers())PlayerUtils.sendTitle(ps, "�4�o�nServer Neustart", "�cDer Server Startet nun Neu!");
		Bukkit.broadcastMessage("�7|> �4Der Server Restartet �7<|");
		task = Bukkit.getScheduler().runTaskTimer(MCAPI.instance, this, 15, 15);
	}

	public void cancel() {
		if (task != null) {
			Bukkit.broadcastMessage("�7|> �4Der Server Restart Wurde Abgebrochen �7<|");
			task.cancel();
		} else {
			System.out.println("Task Never Started!");
		}
	}

}
