package me.Leftwitch.MCAPI.Events;

import lombok.Getter;
import me.Leftwitch.MCAPI.Minigame.CountdownRunnable;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
public class CountdownEndEvent extends Event{
	  private static final HandlerList handlers = new HandlerList();
	  @Getter
	  private CountdownRunnable countdown;
	  
	  public CountdownEndEvent(CountdownRunnable countdown){
		  this.countdown=countdown;
	  }
	  
	  public HandlerList getHandlers()
	  {
	    return handlers;
	  }

	  public static HandlerList getHandlerList()
	  {
	    return handlers;
	  }
	
}
