package me.Leftwitch.MCAPI.Events;

import lombok.Getter;
import me.Leftwitch.MCAPI.Minigame.CountdownRunnable;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.map.MapCanvas;
public class ImageMapRenderEvent extends Event{
	  private static final HandlerList handlers = new HandlerList();
	  @Getter
	  private MapCanvas canvas;
	  
	  public ImageMapRenderEvent(MapCanvas canvas){
		  this.canvas=canvas;
	  }
	  
	  public HandlerList getHandlers()
	  {
	    return handlers;
	  }

	  public static HandlerList getHandlerList()
	  {
	    return handlers;
	  }
	
}
