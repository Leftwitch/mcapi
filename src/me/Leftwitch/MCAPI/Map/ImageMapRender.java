package me.Leftwitch.MCAPI.Map;

import java.awt.image.BufferedImage;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.Events.ImageMapRenderEvent;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ImageMapRender extends MapRenderer {
 
	BufferedImage image;
	public String extratext = "";
	@Override
	public void render(MapView view, MapCanvas can, Player p) {
	  if(extratext.length()>0){
		  MCAPI.values.getRenderThread().render(can, image,extratext);
	  } else {
		  MCAPI.values.getRenderThread().render(can, image);
	  }
	  Bukkit.getPluginManager().callEvent(new ImageMapRenderEvent(can));
		
	}

}
