package me.Leftwitch.MCAPI.Map;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import me.Leftwitch.MCAPI.MCAPI;

import org.bukkit.Bukkit;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MinecraftFont;

import com.google.common.collect.Maps;

public class RenderThread implements Runnable{
	
	HashMap<MapCanvas,Image> torender = Maps.newHashMap();
	HashMap<MapCanvas,String> extraTexts = Maps.newHashMap();

	
	@Override
	public void run(){
		HashMap<MapCanvas, Image> cloned = ((HashMap<MapCanvas, Image>) torender.clone());
		 for(MapCanvas mc : cloned.keySet()){
			 mc.drawImage(0, 0, cloned.get(mc));
			 if(extraTexts.containsKey(mc)){
			 mc.drawText(10, 110, MinecraftFont.Font, extraTexts.get(mc));
			 }
			 torender.remove(mc);
		 
		
		}
	}
	
	
	
	
	
	public void startThread(){
		Bukkit.getScheduler().runTaskTimerAsynchronously(MCAPI.instance, this, 1, 1);
	}
	
	
	public void render(MapCanvas mc , BufferedImage img){
		torender.put(mc, MapPalette.resizeImage(img));
	}
	
	public void render(MapCanvas mc , BufferedImage img,String extrString){
		torender.put(mc, MapPalette.resizeImage(img));
		extraTexts.put(mc, extrString);
	}

}
