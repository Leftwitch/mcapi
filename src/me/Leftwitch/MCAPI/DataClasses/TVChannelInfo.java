package me.Leftwitch.MCAPI.DataClasses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TVChannelInfo {
	String name;
	String currentInTv;
	String time;

}
