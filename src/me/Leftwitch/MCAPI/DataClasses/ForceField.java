package me.Leftwitch.MCAPI.DataClasses;

import lombok.Data;
import me.Leftwitch.MCAPI.Utils.MapGrenze;

import org.bukkit.Effect;
import org.bukkit.Location;

@Data
public class ForceField {
int range = 20;
Location center = null;
MapGrenze mapgrenze;
Effect ef = Effect.FIREWORKS_SPARK;
boolean enabled = true;


}
