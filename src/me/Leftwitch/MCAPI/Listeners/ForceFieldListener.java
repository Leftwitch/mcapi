package me.Leftwitch.MCAPI.Listeners;

import me.Leftwitch.MCAPI.MCAPI;
import me.Leftwitch.MCAPI.DataClasses.ForceField;

import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class ForceFieldListener implements Listener{

	@EventHandler
	public void onMove(PlayerMoveEvent e){
		for(ForceField f : MCAPI.values.getActiveForceFields()){
			if(e.getTo().distance(f.getCenter())>(f.getRange()-2)){
				Vector vec = null;
				double newX = f.getCenter().getX()
						- e.getPlayer().getLocation().getX();
				double newY = f.getCenter().getY()
						- e.getPlayer().getLocation().getY();
				double newZ = f.getCenter().getZ()
						- e.getPlayer().getLocation().getZ();

				Vector v = new Vector(newX, newY, newZ);
				v = v.normalize();
				e.getPlayer().setVelocity(v);
				// e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(-5).setY(1));
				e.getPlayer().sendMessage("�6Du bist auserhalb der reichweite!");
				e.getPlayer().playSound(e.getPlayer().getLocation(),
						Sound.ZOMBIE_WOODBREAK, 3.5F, 3.5F);
			}
		}
	}
	
	
}
