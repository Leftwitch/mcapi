# McApi #
Die McAPI Ermöglicht dir auf viele nützliche oder ausergewöhnliche Funktionen zu Zu greifen.

Die Haupt features Sind:
* Tv-Programm Api
* YouTube API
* Twitch API
* 1.8 (Titles/Tablist)
* Scheduler API
* Name Tag Hiding 

Und vieles vieles mehr!


# Beispiele #
Beispiel Für TV-API:

		
```
#!java

		List<TVChannelInfo> tcil = TVApi
				.getTVChannelInfos(TVApi
						.readContextFromUrl("http://www.tvspielfilm.de/tv-programm/rss/jetzt.xml"));
		for (TVChannelInfo tci : tcil) {
			tci.getName(); // Programm Name
			tci.getCurrentInTv(); // Was gerade Läuft
			tci.getTime(); // Sende Zeit
		}

			    }
```

Minigame API:

```

#!java

		Minigame game = new Minigame(); // Spiel Erstellen
		game.setName("TEST"); // Spiel Name
		game.setAutoRespawn(true); //Auto Respawn
		game.setDropItemsOnDeath(false); // Items Dropen Beim Tot
		game.setFood(true); // Hunger
		game.setBuild(false); // Bauen
		game.setAutoSpawnOnLobby(false); //Immer am Spawn Spawnen
		game.setSpectatorAfterDeath(true); // Spectator nach dem tot
		game.setPrefix("§7[§3Test§7] §3"); // Prefix
		game.load(); // Laden
		game.startLobbyCountdown(false); //Countdown Wird so oder so Gestartet das true false steht für Sync/Async      true = SyncTask, false = Async Task


```


Und Einen Sparenden Scheduler Erstellen:

In den onEnable Teil folgendes:

```
#!java

		MCAPI.registerTimerInterface(new MeineTimerKlasse());

```

Anschließend Erstellen Wir eine Klasse die so aussieht:




```
#!java


import me.Leftwitch.MCAPI.Interfaces.TimerInterface;

public class MeineTimerKlasse implements TimerInterface {


	
}
```


Nun zum Scheduler Selbst:
wir erstellen einfach einen void in Dieser klasse und schreiben 
@SyncTimer oder @AsyncTimer drüber
um den Delay fest zu legen fügen wir klammern hinzu un in diese schreiben wir: intervalInTicks = (TICKS)
Am Ende kann es so Aussehen:



```
#!java

import me.Leftwitch.MCAPI.Annotations.AsyncTimer;
import me.Leftwitch.MCAPI.Interfaces.TimerInterface;

public class MeineTimerKlasse implements TimerInterface {

	@AsyncTimer(intervalInTicks = 5)
	public void void1(){
          Bukkit.broadcastMessage("Alle 5 Ticks");	
	}
	
}
```

Die API ist noch viel umfangreicher probiert einfach mal rum


				